# Anleitung zur Verwendung des Pythonskripts
Das Python Skript dient zum Filtern der ermittelten Spannungen mit dem Zwei-Farben-Quotientenpyrometer.
Aus den gefilterten Daten wird die Temperatur mit Hilfe der entsprechenden Dämpfungskonstante der Glasfaser errechnet.

Falls die Daten als `.tdms` vorliegen, wird die Software [DIAdem](https://www.ni.com/en-us/shop/software/products/diadem.html) von National Instruments benötigt.

## Python
Das Skript basiert auf [Python](https://www.python.org/) und benötigt eine funktionierende Python3 Installation.

Zusätzlich müssen folgende Pakete installiert werden:
`numpy`, `matplotlib`

## Vorgehen und Anwendung
Falls die Daten bereits als `.csv` vorliegen kann mit, dem Filtern und der Temperaturberechnung fortgeführt werden.

### Konvergieren der Daten aus .tdms mit DIAdem in .csv
1. Öffnen der `.tdms` in DIAdem
2. Drag and Drop der `.tdms` Datei in das Datenportal: interne Daten rechts
3. Notieren der eingestellten Werte `Zero`, `Gain` in der entsprechenden Variable des Pythonskripts
4. Löschen aller Einträge bis auf `Channel1`, `Channel2` aus dem Datenportal
5. Exporteiren der Daten als `.csv` mit *Datendatei speichern unter* im Datenportal
6. Verzeichnis des Pythonskripts wählen
7. Dateityp *Textfile - Export (.csv)* auswählen
8. Dateiname festlegen (Dateiname muss mit Variable `file` im Skript übereinstimmen)
9. Speichern
10. Trennzeichen: Tabulator, Dezimalzeichen: Punkt, Kodierung: ANSI

### Filtern und Temperaturberechnung
1. Eingabe der Variablen `Zero`, `Gain`, `Filter`, `sample_rate`, `file`, `file_output`, `Fiber_diameter` und `plot`
2. Python Skript im Verzeichnis ausführen `python Pyrofilter.py`
3. Datei als `file_output.csv` gespeichert. Zusätzlich wird die Anzahl der Einträge (Zeilen) ausgegeben.

## Variablen
Es werden folgende Variablen verwendet:

|Variable|Erläuterung|
|-----|-----|
|`Zero1`|Automatisch eingestellter Zero-Wert aus Kanal 1 des Pyrometers|
|`Zero2`|Automatisch eingestellter Zero-Wert aus Kanal 2 des Pyrometers|
|`Gain1`|Eingestellter Verstärkungsfaktor in Kanal 1 des Pyrometers|
|`Gain2`|Eingestellter Verstärkungsfaktor in Kanal 2 des Pyrometers|
|`Filter_chan1`|Gewählter Filter nach Betrachtung der aufgezeichneten Spannungen in Kanal 1 (Vorschlag: 0.025). Alle Spannungen, die kleiner als der eingestellte Filter sind, werden entfernt.|
|`Filter_chan2`|Gewählter Filter nach Betrachtung der aufgezeichneten Spannungen in Kanal 2 (Vorschlag: 0.020). Alle Spannungen, die kleiner als der eingestellte Filter sind, werden entfernt.|
|`sample_rate`|Eingestellte sample_rate|
|`Fiber_diameter`|Angabe des verwendeten Faserdurchmessers `330` oder `660` wählt entsprechendes Dämpfungspolynom|
|`plot`|Falls `plot` auf `1` gesetzt ist, wird mit dem Paket `matplotlib` eine Übersicht der errechneten Temperaturen ausgegeben|
|`file`|Dateiname des zu filterenden Datensatzes|
|`file_output`|Dateiname der Ausgabedatei|

## Ausgabedatei
Die Ausgabedatei `file_output.csv` enthält in der ersten Spalte den Zeitpunkt nach Messbeginn in Sekunden und in der zweiten Spalte die Temperatur in °C.