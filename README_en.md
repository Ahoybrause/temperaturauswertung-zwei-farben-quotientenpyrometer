# How to use the Python script
The Python script is used to filter the determined voltage with the fiber-optic ratio pyrometer.
Out of the filtered data, the temperature is calculated using the corresponding optical fiber's damping constant.

If the data is only available as `.tdms`, the software [DIAdem](https://www.ni.com/en-us/shop/software/products/diadem.html) from National Instruments is required.

## Python
The script is based on [Python](https://www.python.org/) and requires a running Python3 installation.

In addition, the following packages must be installed:
`numpy`, `matplotlib`

## Procedure and operation
If the data is already available as `.csv`, proceed with filtering and temperature calculation.

### Converge data from .tdms to .csv with DIAdem
1. Open the `.tdms` in DIAdem
2. Drag and Drop the `.tdms` file in the data protal: internal data right
3. Note the set values `Zero`, `Gain` in the Python scrip's corresponding variable
4. Deleting all entries except `Channel1`, `Channel2` out of the data portal
5. Export the data as `.csv` with *save file as* in the data portal
6. Select the directory of the Python script
7. Select file type *Textfile - Export (.csv)*
8. Set filename (filename must match variable `file` in script)
9. Save
10. Separator: Tab, Decimal character: Dot, Encoding: ANSI

### Filtering and temperature calculation
1. Variable's input `Zero`, `Gain`, `Filter`, `sample_rate`, `file`, `file_output`, `Fiber_diameter` and `plot`
2. Run Python script in directory `python Pyrofilter.py`
3. Script saved file as `file_output.csv`. In addition, the number of entries (lines) are displayed.

## Variables
The following variables are used:

|Variable|Declaration|
|-----|-----|
|`Zero1`|Automatically set pyrometer's zero value from channel 1|
|`Zero2`|Automatically set pyrometer's zero value from channel 2|
|`Gain1`|Selected gain factor for the pyrometer's channel 1|
|`Gain2`|Selected gain factor for the pyrometer's channel 2|
|`Filter_chan1`|Selected filter after viewing the recorded voltages in channel 1 (default: 0.025). All voltages smaller than the entered filter are going to be removed.|
|`Filter_chan2`|Selected filter after viewing the recorded voltages in channel 2 (default: 0.02). All voltages smaller than the entered filter are going to be removed.|
|`sample_rate`|Set sample_rate|
|`Fiber_diameter`|Selection of the used fiber diameter `330` or `660` sets corresponding damping polynomial|
|`plot`|If `plot` is set to `1`, the package `matplotlib` gives an overview of the calculated temperatures|
|`file`|Input file name|
|`file_output`|Output file name|

## Output file
The output file `file_output.csv` contains in the first column the time after start of measurement in seconds and in the second column the temperature in °C.