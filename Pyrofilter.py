import numpy as np
import matplotlib.pyplot as plt

Zero1 = 0.00124587273
Zero2 = 0.001492315448
Gain1 = 100000000
Gain2 = 10000000
Filter_chan1 = 0.025
Filter_chan2 = 0.02
sample_rate = 100000
Fiber_diameter = 330 # Damping polynomials for 330µm or 660µm available
plot = 0 # 1 creates plot with package matplotlib for check
file = "test.csv"
file_output = "Ausgabe_neu.csv"

# Filter, data reduction and temperature calculation fiber-optic ratio pyrometer
def Uclear(chan, Zero, Gain):
    return(chan - Zero) / Gain

def clean(chan1, chan2):
    time = np.linspace(0, len(chan1)-1, len(chan1))
    mask1 = chan1 > Filter_chan1
    mask2 = chan2 > Filter_chan2
    return chan1[mask1 & mask2], chan2[mask1 & mask2], time[mask1 & mask2]

def create_new_file(chan1, time, Temperature, file_output):
    output_format = np.empty((len(chan1), 2))
    for i, (tm, t) in enumerate(zip(time, Temperature)):
        output_format[i] = tm, t
        np.savetxt(file_output, output_format, delimiter="\t")
    print("new file created")

def plot(Temperature, plot):
    if plot == 1:
        plt.step(range(0, len(Temperature)), Temperature)

chan1read, chan2read = np.genfromtxt(file, delimiter="\t", skip_header=1, unpack=True)

chan1, chan2, timesteps = clean(chan1read, chan2read)
time = timesteps / sample_rate
Uclear1 = Uclear(chan1, Zero1, Gain1)
Uclear2 = Uclear(chan2, Zero2, Gain2)
print(len(Uclear1))

x = Uclear1/Uclear2

if Fiber_diameter == 330:
    Temperature = 22387.5099*x**5-34301.1638*x**4+24192.2082*x**3-7864.2893*x**2+2889.6691*x-56.9044
    plot(Temperature, plot)
    create_new_file(chan1, time, Temperature, file_output)
elif Fiber_diameter == 660:
    Temperature = 14511.5409906668*x**5-23493.0581861909*x**4+17827.4193515881*x**3-5862.8715935949*x**2+2516.2817922297*x-31.1445912162
    plot(Temperature, plot)
    create_new_file(chan1, time, Temperature, file_output)
else:
    print("No valid fiber diameter")